//
//  LoginService.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 29/08/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation
import UIKit

class LoginService: APIRequest {

    @discardableResult
    static func createUser(email: String, password: String, name: String, phone: String, course: String, completion: ResponseBlock<Any>?) -> LoginService {
        let params = ["email": email, "password": password, "phone": phone, "name": name, "course": course]
        let request = LoginService(method: .post, path: "user/", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                let user = User(dictionary: response)
                completion?(user, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func updateUser(id: Int, email: String, password: String, name: String, phone: String, course: String, completion: ResponseBlock<Any>?) -> LoginService {
        let params = ["id": id, "email": email, "password": password, "name": name, "phone": phone, "course": course] as [String : Any]
        let request = LoginService(method: .put, path: "user", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                completion?(response, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func deleteUser(id: Int, completion: ResponseBlock<Any>?) -> LoginService {
        let params = ["id": id]
        let request = LoginService(method: .delete, path: "user", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                let user = User(dictionary: response)
                completion?(user, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func AuthLogin(email: String, password: String, completion: ResponseBlock<Any>?) -> LoginService {
        let params = ["email": email, "password": password]
        let request = LoginService(method: .post, path: "user/login", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                let user = User(dictionary: response)
                completion?(user, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func getUser(id: Int, completion: ResponseBlock<Any>?) -> LoginService {
        let request = LoginService(method: .get, path: "user/?id=\(id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                let user = User(dictionary: response)
                completion?(user, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
}
