//
//  LikeService.swift
//  LDS
//
//  Created by Rafaela Dutra on 31/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation
import UIKit

class LikeService: APIRequest {
    
    @discardableResult
    static func like(idUser: Int, idPost: Int, completion: ResponseBlock<Any>?) -> LikeService {
        let params = ["PostId": idPost, "UserId": idUser]
        let request = LikeService(method: .post, path: "post/like", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                completion?(response, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func undoLike(idUser: Int, idPost: Int, completion: ResponseBlock<Any>?) -> LikeService {
        let params = ["PostId": idPost, "UserId": idUser]
        let request = LikeService(method: .delete, path: "post/like", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                completion?(response, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
}
