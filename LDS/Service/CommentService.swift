//
//  CommentService.swift
//  LDS
//
//  Created by Rafaela Dutra on 31/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation
import UIKit

class CommentService: APIRequest {
    
    @discardableResult
    static func addComment(idUser: Int, idPost: Int, comment: String, completion: ResponseBlock<Any>?) -> CommentService {
        let params = ["PostId": idPost, "UserId": idUser, "comment": comment] as [String : Any]
        let request = CommentService(method: .post, path: "post/comment", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                completion?(response, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func deletComment(id: Int, completion: ResponseBlock<Any>?) -> CommentService {
        let request = CommentService(method: .delete, path: "post/comment?id=\(id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                completion?(response, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func getComment(id: Int, completion: ResponseBlock<Any>?) -> CommentService {
        let request = CommentService(method: .get, path: "post/comment?PostId=\(id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                var listComment: [Commet] = []
                guard let responseData = response as? [JSONDictionary] else { return }
                for element in responseData {
                    listComment.append(Commet(dictionary: element))
                }
                completion?(listComment, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
}
