//
//  PostService.swift
//  LDS
//
//  Created by Rafaela Dutra on 31/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation
import UIKit

class PostService: APIRequest {
    
    @discardableResult
    static func createPost(description: String, idGroup: Int, idOwner: Int, completion: ResponseBlock<Any>?) -> PostService {
        let params = ["body": description, "GroupId": idGroup, "OwnerId": idOwner] as [String : Any]
        let request = PostService(method: .post, path: "post", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                let post = Post(dictionary: response)
                completion?(post, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func deletePost(id: Int, completion: ResponseBlock<Any>?) -> PostService {
        let request = PostService(method: .delete, path: "post/?id=\(id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                completion?(response, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func listPost(completion: ResponseBlock<Any>?) -> PostService {
        let request = PostService(method: .get, path: "post", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                var listPost: [Post] = []
                guard let responseData = response as? [JSONDictionary] else { return }
                for element in responseData {
                    listPost.append(Post(dictionary: element))
                }
                completion?(listPost, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func listOnePost(id: Int, completion: ResponseBlock<Any>?) -> PostService {
        let request = PostService(method: .get, path: "post/?id=\(id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                let post = Post(dictionary: response)
                completion?(post, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
}
