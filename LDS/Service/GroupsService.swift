//
//  GroupsService.swift
//  LDS
//
//  Created by Rafaela Dutra on 30/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation
import UIKit

class GroupService: APIRequest {
    
    @discardableResult
    static func createGroup(name: String, description: String, completion: ResponseBlock<Any>?) -> GroupService {
        let params = ["name": name, "description": description]
        let request = GroupService(method: .post, path: "group", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let response = response as? JSONDictionary else { return }
                let group = Group(dictionary: response)
                completion?(group, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func getGroupUser(id: Int, completion: ResponseBlock<Any>?) -> GroupService {
        let request = GroupService(method: .get, path: "group/user/?groupId=\(id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                guard let listGroup = response as? JSONDictionary else { return }
                let group = Group(dictionary: listGroup)
                completion?(group, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func getGroup(completion: ResponseBlock<Any>?) -> GroupService {
        let request = GroupService(method: .get, path: "group", parameters: nil, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                var groupList: [Group] = []
                guard let listGroup = response as? [JSONDictionary] else { return }
                for elemente in listGroup {
                    groupList.append(Group(dictionary: elemente))
                }
                completion?(groupList, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func followGroup(userId: Int, groupId: Int, completion: ResponseBlock<Any>?) -> GroupService {
        let params = ["groupId": groupId, "userId": userId]
        let request = GroupService(method: .post, path: "group/user", parameters: params, urlParameters: nil, cacheOption: .networkOnly, useClientAPI: true, extraHeaders: nil) { (response, error, cache) in
            if error == nil {
                completion?(response, error, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
}
