//
//  EditViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 05/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var courseTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    //MARK: - Variables
    
    var user: User!
    
    //MARK: - Life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveButton.layer.cornerRadius = 5
        self.setInformation()
        self.activity.isHidden = true
    }
    
    func setInformation() {
        nameTextField.text = user.name
        emailTextField.text = user.email
        phoneTextField.text = user.phone
        courseTextField.text = user.course
        
    }
    
    //MARK: - IBAction
    
    @IBAction func deleteUser(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Tem certeza que quer deletar sua conta?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Sim", style: UIAlertAction.Style.default, handler: { action in
            if let vc = UIStoryboard.init(name: "Group", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewControllerID") as? HomeViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "Não", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func save(_ sender: Any) {
        activity.isHidden = false
        activity.startAnimating()
        LoginService.updateUser(id: user.identifier, email: emailTextField.text ?? user.email!, password: user.password, name: nameTextField.text ?? user.name, phone: phoneTextField.text ?? user.phone, course: courseTextField.text ?? user.course, completion: { (user, error, _) in
            self.activity.stopAnimating()
            self.activity.isHidden = true
            if error == nil {
                if let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewControllerID") as? ProfileViewController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        })
        
    }
    
}
