//
//  HomeViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 19/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var nameProfile: UILabel!
    @IBOutlet weak var courseProfile: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewProfile: UIView!
    
    var user: User!
    var listPosts: [Post] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setProfile()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        self.loadGroup()
        self.viewProfile.layer.cornerRadius = 5
        
//        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        if !inspection.instructions!.isEmpty {
//
//            alert.addAction(UIAlertAction(title: "Instructions".localized(), style: .default, handler: { (UIAlertAction) in
//
//                self.showAlertViewWithTitle(title: ("Instructions").localized(), message: self.inspection.instructions!.localized())
//
//            }))
//        }
//
//
//        alert.addAction(UIAlertAction(title: "Create WO".localized(), style: .default, handler: { (UIAlertAction) in
//            self.performSegue(withIdentifier: "createWOFromInspection", sender: nil)
//        }))
//
//        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (UIAlertAction) in
//        }))
//
//        self.present(alert, animated: true, completion: {
//        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreatePublication"{
            (segue.destination as! GroupCreatePublicationViewControlle).user = user
            (segue.destination as! GroupCreatePublicationViewControlle).allGroups = true
        } else {
            (segue.destination as! GroupCreatePublicationViewControlle).user = user
            (segue.destination as! GroupCreatePublicationViewControlle).allGroups = false

        }
        
    }
    
    func loadGroup() {
        PostService.listPost { (post, error, _) in
            if error == nil {
                    self.listPosts = post as! [Post]
                    self.tableView.reloadData()
            }
        }
    }
    
    func setProfile() {
        nameProfile.text = user.name
        courseProfile.text = user.course
        
    }
    
    @IBAction func didTapProfile(_ sender: Any) {
        if let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewControllerID") as? ProfileViewController {
            vc.user = user
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapListGroup(_ sender: Any) {
        if let vc = UIStoryboard.init(name: "Group", bundle: Bundle.main).instantiateViewController(withIdentifier: "GroupViewControllerID") as? GroupViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostsTableViewCell
        cell.details = listPosts[indexPath.row].descriptionPost!
        cell.post = listPosts[indexPath.row]
        cell.delegate = self
        cell.setPosts()
        return cell
    }
}

extension HomeViewController: PostCellProtocol {
    func setLike(post: Post) {
        LikeService.like(idUser: user.identifier, idPost: post.id) { (like, error, _) in
            if error == nil {
                
            }
        }
    }
    
    func removeLike(post: Post) {
        LikeService.undoLike(idUser: user.identifier, idPost: post.idOwner) { (like, error, _) in
            if error == nil {
            }
        }
    }
    
    func setUnlike(post: Post) {
        UnlikeService.unlike(idUser: user.identifier, idPost: post.idOwner) { (unlike, error, _) in
            if error == nil {
            }
        }
    }
    
    func removeUnlike(post: Post) {
        UnlikeService.deletUnlike(idUser: user.identifier, idPost: post.idOwner) { (unlike, error, _) in
            if error == nil {
            }
        }
    }
    
    func setComment(post: Post) {
        if let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "CommentViewControllerID") as? CommentViewContoller {
            vc.post = post
            vc.user = self.user
            self.present(vc, animated: true)
        }
    }
    
    
}
