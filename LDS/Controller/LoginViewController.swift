//
//  LoginViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 29/08/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    
    @IBAction func register(_ sender: Any) {
    }
    
    @IBAction func login(_ sender: Any) {
        LoginService.AuthLogin(email: emailTextfield.text!, password: passwordTextfield.text!) { (response, error, _) in
            if error == nil {
                    if let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewControllerID") as? HomeViewController {
                        vc.user = response as! User
                        self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
    }
    
}

