//
//  GroupCreatePublicationViewController.swift
//  LDS
//
//  Created by Rafaela Dutra on 26/11/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class GroupCreatePublicationViewControlle: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var groupList: [Group] = []
    var user: User!
    var allGroups: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        if allGroups {
            setListGroup()
        } else {
            setList()
        }
        
    }
    
    func setListGroup() {
        GroupService.getGroupUser(id: user.identifier) { (group, error, _) in
            if error == nil {
                self.groupList.append(group as! Group)
                self.tableView.reloadData()
            }
        }
    }
    
    func setList() {
        GroupService.getGroup { (group, error, _) in
            if error == nil {
                self.groupList = group as! [Group]
                self.tableView.reloadData()
            }
        }
    }
}

extension GroupCreatePublicationViewControlle: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellGroup", for: indexPath) as? GroupTableViewCell {
            cell.group = groupList[indexPath.row]
            cell.setInformation()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellGroup", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !allGroups {
            GroupService.followGroup(userId: user.identifier, groupId: groupList[indexPath.row].id) { (response, error, _) in
                if error == nil {

                }
            }
        } else {
            if let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreatePublicationViewControllerID") as? CreatePublicationViewController {
                vc.group = self.groupList[indexPath.row]
                vc.user = self.user
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
