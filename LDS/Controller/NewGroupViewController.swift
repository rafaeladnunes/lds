//
//  NewGroupViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 19/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class NewGroupViewController: UIViewController {
    
    @IBOutlet weak var nameGroupField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func save(_ sender: Any) {
        GroupService.createGroup(name: nameGroupField.text!, description: descriptionField.text!) { (group, error, _) in
            if error == nil {
                self.navigationController?.popViewController(animated: true)

            }
        }
    }
}
