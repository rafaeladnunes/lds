//
//  GroupViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 12/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class GroupViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variable
    
    var groupList: [Group] = []
    
    //MARK: - Life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
        tableView.delegate = self
        tableView.dataSource = self
        self.setList()
    }
    
    func setList() {
        GroupService.getGroup { (group, error, _) in
            if error == nil {
                self.groupList = group as! [Group]
                self.tableView.reloadData()
            }
        }
    }

    @IBAction func addGroup(_ sender: Any) {
        
    }
}

extension GroupViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellGroup", for: indexPath) as? GroupTableViewCell {
            cell.group = self.groupList[indexPath.row]
            cell.setInformation()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellGroup", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "", message: "Deseja seguir esse grupo?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Sim", style: UIAlertAction.Style.default, handler: { action in
            print("foi")
        }))
        alert.addAction(UIAlertAction(title: "Não", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
