//
//  RegisterViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 29/08/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var courseTextField: UITextField!
    
//    let service = LoginService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cadastro"
        
    }
    
    @IBAction func save(_ sender: Any) {
        LoginService.createUser(email: emailTextField.text!, password: passwordTextField.text!, name: nameTextField.text!, phone: phoneTextField.text!, course: courseTextField.text!) { (user, error, _) in
            if error == nil {
                if let user = user as? User {
                    if let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewControllerID") as? HomeViewController {
                        vc.user = user
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
            }
        }
        
    }
    
}
