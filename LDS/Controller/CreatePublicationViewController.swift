//
//  CreatePublicationViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 23/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class CreatePublicationViewController: UIViewController {
    
    @IBOutlet weak var descriptionText: UITextView!
    
    var listPost: [Post] = []
    var group: Group!
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func publication(_ sender: Any) {
        if let details = descriptionText.text {
            PostService.createPost(description: details, idGroup: group.id, idOwner: user.identifier) { (post, error, _) in
                if error == nil {
                    if let newPost = post as? Post {
                        self.listPost.append(newPost)
                    }
                }
            }
        }
        
        
        
        if let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewControllerID") as? HomeViewController {
            vc.listPosts = listPost
            vc.user = user
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
