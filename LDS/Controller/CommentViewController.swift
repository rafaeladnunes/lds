//
//  CommentViewController.swift
//  LDS
//
//  Created by Rafaela Dutra on 31/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class CommentViewContoller: UIViewController {
    
    @IBOutlet weak var commentUser: UITextField!
    @IBOutlet weak var textComment: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonBack: UIButton!
    
    var user: User!
    var post: Post!
    var listComment: [Commet] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack.layer.cornerRadius = 4
        self.getComment()
    }
    
    func getComment() {
        CommentService.getComment(id: post.id) { (response, error, _) in
            if error == nil {
                self.listComment = response as! [Commet]
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendComment(_ sender: Any) {
        if let comment = commentUser.text {
            CommentService.addComment(idUser: user.identifier, idPost: post.id, comment: comment) { (comment, error, _) in
                if error == nil {
                    self.getComment()
                    self.commentUser.text = ""
                    
                }
            }
        }
        
    }
    
}

extension CommentViewContoller: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentTableViewCell {
            cell.comment = listComment[indexPath.row]
            cell.user = self.user
            cell.setupComment()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath)
            return cell
        }
    }
    
}
