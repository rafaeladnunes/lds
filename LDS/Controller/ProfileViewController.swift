//
//  ProfileViewController.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 05/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var courseLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var numberPoints: UILabel!
    
    
    //MARK: - Variables
    
    var groupList: [Group] = []
    var user: User!
    var name: String = ""
    var phone: String = ""
    var service: GroupService!
    
    //MARK: - Life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editButton.layer.cornerRadius = 5
        self.setInformation()
        self.getUser()
    }

    func getUser() {
        LoginService.getUser(id: user.identifier) { (response, error, _) in
            if error == nil {
                self.user = response as! User
                self.setInformation()
            }
        }
    }
    
    func setInformation() {
        nameLabel.text = user.name
        phoneLabel.text = user.phone
        emailLabel.text = user.email
        courseLabel.text = user.course
        numberPoints.text = "você tem \(user.points) pontos"
    }
    
    //MARK: - IBAction
    
    @IBAction func didTapEdit(_ sender: Any) {
        if let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditViewControllerID") as? EditViewController {
            vc.user = self.user
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
