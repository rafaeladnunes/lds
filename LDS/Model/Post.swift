//
//  Post.swift
//  LDS
//
//  Created by Rafaela Dutra on 31/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation

struct Post: Mappable {
    
    
    var idGroup: Int
    var id: Int
    var descriptionPost: String?
    var idOwner: Int
    var likes: Int
    var unlikes: Int
    var points: Int
    
    var originalDictionary: JSONDictionary
    
    init(mapper: Mapper) {
        self.idGroup = mapper.keyPath("GroupId")
        self.descriptionPost = mapper.keyPath("body")
        self.idOwner = mapper.keyPath("OwnerId")
        self.id = mapper.keyPath("id")
        self.likes = mapper.keyPath("likes")
        self.unlikes = mapper.keyPath("unlikes")
        self.points = mapper.keyPath("points")
        
        self.originalDictionary = ["id": self.id, "GroupId": self.idGroup, "description": self.descriptionPost, "OwnerId": self.idOwner, "likes": self.likes, "unlikes": self.unlikes, "points": self.points]
    }
    
}
