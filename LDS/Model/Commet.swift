//
//  Commet.swift
//  LDS
//
//  Created by Rafaela Dutra on 31/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation

struct Commet: Mappable {
    
    var idUser: Int
    var idPost: Int?
    var comment: String
    
    var originalDictionary: JSONDictionary
    
    init(mapper: Mapper) {
        self.idPost = mapper.keyPath("PostId")
        self.idUser = mapper.keyPath("UserId")
        self.comment = mapper.keyPath("comment")
        
        self.originalDictionary = ["PostId": self.idPost, "UserId": self.idUser, "comment": self.comment]
    }
    
}

