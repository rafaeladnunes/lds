//
//  Like.swift
//  LDS
//
//  Created by Rafaela Dutra on 31/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation

struct Like: Mappable {
    
    
    var idUser: Int
    var idPost: Int?
    
    var originalDictionary: JSONDictionary
    
    init(mapper: Mapper) {
        self.idPost = mapper.keyPath("PostId")
        self.idUser = mapper.keyPath("UserId")
        
        self.originalDictionary = ["PostId": self.idPost, "UserId": self.idUser]
    }
    
}

