//
//  User.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 05/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation

struct Group: Mappable {
    
    
    var name: String
    var id: Int
    var descriptionGroup: String?
    
    var originalDictionary: JSONDictionary
    
    init(mapper: Mapper) {
        self.name = mapper.keyPath("name")
        self.descriptionGroup = mapper.keyPath("description")
        self.id = mapper.keyPath("id")
        
        self.originalDictionary = ["id": self.id, "name": self.name, "description": self.descriptionGroup ?? ""]
    }
    
}
