//
//  User.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 05/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import Foundation

struct User: Mappable {
    
    
    var identifier: Int
    var email: String?
    var name: String
    var phone: String
    var course: String
    var password: String
    var points: Int
    
    var originalDictionary: JSONDictionary
    
    init(mapper: Mapper) {
        self.identifier = mapper.keyPath("id")
        self.email = mapper.keyPath("email")
        self.name = mapper.keyPath("name")
        self.phone = mapper.keyPath("phone")
        self.course = mapper.keyPath("course")
        self.password = mapper.keyPath("password")
        self.points = mapper.keyPath("points")
        
        self.originalDictionary = ["id": self.identifier, "name": self.name,
                                   "email": self.email, "phone": self.phone,
                                   "courses": self.course,
                                   "password": self.password, "points": self.points]
    }

}
