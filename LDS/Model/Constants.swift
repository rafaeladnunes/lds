//
//  Constants.swift
//  Timcoo-cliente
//
//  Created by Lucas Valle on 03/11/17.
//  Copyright © 2017 Timcoo. All rights reserved.
//

import UIKit

typealias JSONDictionary = [String: Any]
typealias JSONArray = [[String: Any]]

struct Constants {
}

extension UserDefaults {
    enum Keys {
        static var Team = "Team"
        static var UserLogged = "UserLogged"
        static var DeviceToken = "DeviceToken"
    }
}

extension Notification.Name {
    static let nextCardNotification = Notification.Name("nextCardNotification")
    static let updateScoreContent = Notification.Name("updateScoreContent")
    static let lastCardNotification = Notification.Name("lastCardNotification")
    static let kAVPlayerVCDismissingNotification = Notification.Name.init("dismissing")
}
