//
//  GroupTableViewCell.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 05/09/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameGroupLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var group: Group!
    
    func setInformation() {
        nameGroupLabel.text = group.name
        descriptionLabel.text = group.descriptionGroup
        
    }
    
    
}
