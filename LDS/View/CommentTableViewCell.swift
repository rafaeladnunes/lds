//
//  CommentTableViewCell.swift
//  LDS
//
//  Created by Rafaela Dutra on 26/11/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    var comment: Commet!
    var user: User!
    
    func setupComment() {
        commentLabel.text = comment.comment
        nameUser.text = user.name
    }
    
}
