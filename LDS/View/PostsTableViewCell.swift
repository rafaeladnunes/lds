//
//  PostsTableViewCell.swift
//  LDS
//
//  Created by Rafaela Dutra Nunes on 17/10/19.
//  Copyright © 2019 Rafaela Dutra Nunes. All rights reserved.
//

import UIKit

protocol PostCellProtocol: class {
    func setComment(post: Post)
    func setLike(post: Post)
    func removeLike(post: Post)
    func setUnlike(post: Post)
    func removeUnlike(post: Post)
}

class PostsTableViewCell: UITableViewCell {
    
    var details: String = ""
    var post: Post!
    var selectLike: Bool = false
    var selectUnlike: Bool = false
    weak var delegate: PostCellProtocol?
    
    
    @IBOutlet weak var detailsPost: UITextView!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var buttonUnlike: UIButton!
    @IBOutlet weak var numberLikes: UILabel!
    @IBOutlet weak var numberUnlike: UILabel!
    @IBOutlet weak var numberPonto: UILabel!
    
    func setPosts() {
        detailsPost.text = post.descriptionPost
        numberLikes.text = "\(post.likes) curtidas"
        numberUnlike.text = "\(post.unlikes) não gostei"
        numberPonto.text = "\(post.points) pontos"
    }
    
    @IBAction func setUnlike(_ sender: Any) {
        if !selectUnlike {
            self.delegate?.setUnlike(post: post)
            numberUnlike.text = "\(post.unlikes+1) não gostei"
            selectUnlike = true
        } else {
            self.delegate?.removeUnlike(post: post)
            numberUnlike.text = "\(post.unlikes-1) não gostei"
            selectUnlike = false
        }
        
    }
    @IBAction func setLike(_ sender: Any) {
        if !selectLike {
            self.delegate?.setLike(post: post)
            numberLikes.text = "\(post.likes+1) curtidas"
            selectLike = true
        } else {
            self.delegate?.removeLike(post: post)
            numberLikes.text = "\(post.likes-1) curtidas"
            selectLike = false
        }
    }
    @IBAction func setComment(_ sender: Any) {
        self.delegate?.setComment(post: post)
    }
}
